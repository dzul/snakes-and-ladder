//: Playground - noun: a place where people can play

import UIKit

var board:[Int:Int] = [3 : 8, 13 : -10, 38 : 10, 50 : -8, 60 : 13, 75 : -10, 89 : 5]
var players:[String:Int] = ["player 1":0,"player 2":0]
var rollDice:Int = 0
var end:Int = 100
var hasWinner:Bool = false
var currRound: Int = 1

//roll dice
func rollTheDice() -> Int{
    rollDice = Int(arc4random_uniform(6)) + 1
    print("Dice rolled is \(rollDice)")
    return rollDice
}

//loops until win
while (!hasWinner){
    
    print("Round Number: \(currRound)")
    
    for (player , currentPosition) in players {
        
        print("\(player)")
        rollTheDice()
        
        var latestPosition:Int = currentPosition + rollDice
        print("\(player) in position \(latestPosition)")
        
        if let action:Int = board[latestPosition]{
            if action > 0 {
                print("Player stepped on ladder: Up \(action) steps")
            }else{
                print("Player stepped on snakess: Down \(action) steps")
            }
            latestPosition += action
        }
        
    //print position of the players
    print("\(player) in \(latestPosition) position")
        players[player] = latestPosition
        
        if latestPosition >= 100{
            hasWinner = true
            print("\n Congrats \(player), you have won")
            break
        }
        
    }
    
    currRound++
    

    
}
